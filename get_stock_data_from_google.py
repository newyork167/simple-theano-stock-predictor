import pandas as pd
import requests
from io import StringIO
import datetime
import time
from dateutil import tz

min_base_url = 'https://www.google.com/finance/getprices?i=60&p=10d&f=d,o,h,l,c,v&df=cpct&q={ticker}'

stock_tickers = ['aapl']

should_save = True
save_as_test = True

last_date = None
from_zone = tz.tzutc()
to_zone = tz.gettz('America/New_York')


def calculate_date(row):
    global last_date
    date_to_return = None
    if 'a' in row['DATE']:
        last_date = datetime.datetime.fromtimestamp(float(row['DATE'].split('a')[-1]))
        date_to_return = last_date
    else:
        this_date = last_date + datetime.timedelta(seconds=60 * float(row['DATE']))
        date_to_return = this_date

    date_to_return = date_to_return.replace(tzinfo=from_zone)
    date_to_return = date_to_return.astimezone(to_zone)
    return date_to_return.strftime("%Y-%m-%d %H:%M:%S")

for ticker in stock_tickers:
    print("Getting stock ticker data for {ticker}".format(ticker=ticker))
    csv_data = requests.get(min_base_url.format(ticker=ticker.upper())).text
    csv_data = "\n".join([csv_data.split('\n')[4], "\n".join(csv_data.split('\n')[7:])])
    df = pd.read_csv(StringIO(csv_data))
    df = df.rename(index=str, columns={"COLUMNS=DATE": "DATE"})
    df['date'] = df.apply(lambda row: calculate_date(row), axis=1)
    del df['DATE']
    df.columns = map(str.lower, df.columns)
    cols = df.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    df = df[cols]
    print(df.tail(100))
    globals()[ticker + "_stock_df"] = df

if should_save:
    for ticker in stock_tickers:
        df = globals()[ticker + "_stock_df"]
        df.to_csv("Datasets/{0}_min_stocks{1}.csv".format(ticker, "_test" if save_as_test else ""))
