import pandas as pd
import sys
from sklearn.model_selection import train_test_split
import seaborn as sns
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers import Merge
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, CSVLogger, EarlyStopping
from keras.optimizers import RMSprop, Adam, SGD, Nadam
from keras.layers.advanced_activations import *
from keras.layers import Convolution1D, MaxPooling1D, AtrousConvolution1D
from keras.layers.recurrent import LSTM, GRU
from keras import regularizers
from keras.utils.np_utils import to_categorical
import theano
import numpy as np
import os
import nn
import rnn

pickle_path = "Pickle/{type}.pickle"
update_pickle = True

pd.set_option('display.float_format', lambda x: '%.2f' % x)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

sns.despine()

theano.config.compute_test_value = "ignore"


def get_new_model(input_dim, initial_dense_dim=64):
    model = Sequential()
    model.add(Dense(initial_dense_dim, input_dim=input_dim, activity_regularizer=regularizers.l2(0.01)))
    model.add(BatchNormalization())
    model.add(LeakyReLU())
    model.add(Dropout(0.50))
    model.add(Dense(2))
    model.add(Activation('softmax'))
    return model


def calculate_last_close(row):
    predictor = 0
    if float(row['close']) > float(row['previous_close']):
        predictor = 1
    return predictor


previous_close = 0


def set_previous_close(row):
    global previous_close
    if previous_close == 0:
        previous_close = float(row['close'])
    close_to_return = previous_close
    previous_close = float(row['close'])
    return close_to_return


insert_prediction_count = 0


def insert_prediction_data(row, predictions):
    global insert_prediction_count
    prediction = predictions[insert_prediction_count]
    insert_prediction_count += 1
    return int(prediction[1] > prediction[0])


columns_to_drop = ['Unnamed: 0', 'date', 'volume']
pretty_column_ordering = ['open', 'high', 'mid', 'low', 'close', 'previous_close', 'date']


# Return (test, train)
def split_data(file_path, split=0.3):
    df = pd.read_csv(file_path)
    df['mid'] = df.apply(lambda row: float(row['low']) + (float(row['high']) - float(row['low'])) / 2.0, axis=1)
    df['previous_close'] = df.apply(lambda row: set_previous_close(row), axis=1)
    df = df[pretty_column_ordering]
    df['predictor'] = df[::-1].apply(lambda row: calculate_last_close(row), axis=1)[::-1]
    # df = df[df.columns.difference(columns_to_drop)]
    return [df] + train_test_split(df[df.columns.difference(columns_to_drop)], test_size=split)


def get_test_to_predict(file_path, train_df_last_date):
    test_date = train_df_last_date['date'].values[0]
    print("Getting test dates since {}".format(test_date))
    df = pd.read_csv(file_path)
    df['mid'] = df.apply(lambda row: float(row['low']) + (float(row['high']) - float(row['low'])) / 2.0, axis=1)
    df['previous_close'] = df.apply(lambda row: set_previous_close(row), axis=1)
    df = df[pretty_column_ordering]
    test_df = df[df['date'] > test_date]
    return test_df[test_df.columns.difference(columns_to_drop)]


def load_dataframes():
    # Added this to save computation time
    if not os.path.exists(pickle_path.format(type="df")) or update_pickle:
        df, test, train = split_data('Datasets/aapl_min_stocks.csv')
        final_test_df = get_test_to_predict("Datasets/aapl_min_stocks_test.csv", df.iloc[-1:])
        for t, d in {"df": df, "test": test, "train": train, "final": final_test_df}.items():
            d.to_pickle(pickle_path.format(type=t))
    else:
        df = pd.read_pickle(pickle_path.format(type="df"))
        test = pd.read_pickle(pickle_path.format(type="test"))
        train = pd.read_pickle(pickle_path.format(type="train"))
        final_test_df = pd.read_pickle(pickle_path.format(type="final"))
    return [df, test, train, final_test_df]


def train_neural1():
    df, test, train, simulate = load_dataframes()
    predictor_columns = ['predictor']
    prediction_columns_to_drop = []

    model = get_new_model(len(train.columns) - len(prediction_columns_to_drop) - 1, initial_dense_dim=len(train.index))
    opt = Nadam(lr=0.001)

    reduce_lr = ReduceLROnPlateau(monitor='val_acc', factor=0.9, patience=25, min_lr=0.000001, verbose=1)
    checkpointer = ModelCheckpoint(filepath="test.hdf5", verbose=1, save_best_only=True)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

    y_train = train[predictor_columns].values
    x_train = train[[x for x in train.columns if x not in predictor_columns and x not in prediction_columns_to_drop]].values
    y_test = test[predictor_columns].values
    x_test = test[[x for x in test.columns if x not in predictor_columns and x not in prediction_columns_to_drop]].values

    early_stopping_monitor = EarlyStopping(patience=4)

    history = model.fit(x_train, to_categorical(y_train),
                        nb_epoch=100,
                        batch_size=128,
                        verbose=1,
                        validation_data=(x_test, to_categorical(y_test)),
                        callbacks=[reduce_lr, checkpointer, early_stopping_monitor],
                        shuffle=True)

    results = model.predict(simulate[[x for x in simulate.columns if x not in predictor_columns and x not in prediction_columns_to_drop]].values)

    # Test accuracy of model
    simulate["prediction"] = simulate.apply(lambda row: insert_prediction_data(row, results), axis=1)
    simulate["actual"] = simulate.apply(lambda row: int(row["close"] > row['previous_close']), axis=1)
    simulate['result'] = simulate.apply(lambda row: int(row['prediction'] == row['actual']), axis=1)
    print(simulate.head(15))
    print("\nAccuracy: {}".format(simulate['result'].sum() / len(simulate.index)))


def train_neural2(test, train):
    predictor_columns = ['predictor']
    prediction_columns_to_drop = ['close']
    y_train = train[predictor_columns].values
    x_train = train[
        [x for x in train.columns if x not in predictor_columns and x not in prediction_columns_to_drop]].values
    y_test = test[predictor_columns].values
    x_test = test[
        [x for x in test.columns if x not in predictor_columns and x not in prediction_columns_to_drop]].values

    test_data = rnn.Data(x_test, y_test)
    train_data = rnn.Data(x_train, y_train)

    r = rnn.RNN(len(x_train), 50, x_train.shape[1], y_train.shape[1])
    n = nn.NeuralNetwork(nn=r,
                         validation_split=0.2,
                         batch_size=256,
                         nb_epoch=10,
                         show_accuracy=True)
    n.train(train_data)
    n.test(test_data)
    n.run_with_cross_validation(test_data, 2)
    features = n.feature_selection(test_data)
    print(features)


train_neural1()
# train_neural2(test, train)
